/**
 * D8OX Leaflet Grouped Styled Layers.
 *
 * This is script is a fork of
 * {@link https://github.com/davicustodio/Leaflet.StyledLayerControl|Leaflet.StyledLayerControl},
 * by @davicustodio. It has been modified to include support for buttons and
 * sliders controls for layer.
 *
 * @see {@link https://github.com/davicustodio/Leaflet.StyledLayerControl|Leaflet.StyledLayerControl}
 * @see {@link https://github.com/davicustodio/Leaflet.StyledLayerControl/issues/26|Patch: better id and classes for improved styling}
 *
 * @version 1.0
 * @author <a href='https://mexapi.macpress.com.br/about'>@denydias</a>
 * @license GPL-2.0+
 * @copyright ©2016 Instituto Socioambiental
 *
 * Enhancements by silvio@socioambiental.org.
 */

L.Control.StyledLayerControl = L.Control.Layers.extend({
  options: {
    collapsed  : true,
    position   : 'topright',
    autoZIndex : true,
    debug      : false,
  },

  initialize: function(baseLayers, groupedOverlays, options) {
    var i, j;

    L.Util.setOptions(this, options);

    this._layers        = {};
    this._layerSliders  = {};
    this._groupList     = [];
    this._domGroups     = [];
    this._lastZIndex    = 0;
    this._controlState  = {};
    this._handlingClick = false;

    var order = 0;
    for (i in baseLayers) {
      for (j in baseLayers[i].layers) {
        baseLayers[i].layers[j].groupElementId = i;
        this._addLayer(baseLayers[i].layers[j], j, baseLayers[i], false, ++order);
      }
    }

    for (i in groupedOverlays) {
      for (j in groupedOverlays[i].layers) {
        groupedOverlays[i].layers[j].groupElementId = i;
        this._addLayer(groupedOverlays[i].layers[j], j, groupedOverlays[i], true, ++order);
      }
    }
  },

  onAdd: function(map) {
    this._initLayout();
    this._update();

    map
      .on('layeradd',    this._onLayerChange, this)
      .on('layerremove', this._onLayerChange, this);

    return this._container;
  },

  onRemove: function(map) {
    map
      .off('layeradd',    this._onLayerChange)
      .off('layerremove', this._onLayerChange);
  },

  addBaseLayer: function(layer, name, group) {
    this._addLayer(layer, name, group, false);
    this._update();
    return this;
  },

  addOverlay: function(layer, name, group) {
    this._addLayer(layer, name, group, true);
    this._update();
    return this;
  },

  removeLayer: function(layer) {
    var id = L.Util.stamp(layer);

    delete this._layers[id];
    this._update();

    return this;
  },

  removeGroup: function(group_Name) {
    for (var group in this._groupList) {
      if (this._groupList[group].groupName == group_Name) {
        for (var layer in this._layers) {
          if (this._layers[layer].group && this._layers[layer].group.name == group_Name) {
            delete this._layers[layer];
          }
        }

        delete this._groupList[group];
        this._update();
        break;
      }
    }
  },

  selectLayer: function(layer) {
    this._map.addLayer(layer);
    this._update();
  },

  unSelectLayer: function(layer) {
    this._map.removeLayer(layer);
    this._update();
  },

  selectGroup: function(group_Name){
    this.changeGroup(group_Name, true);
  },

  unSelectGroup: function(group_Name){
    this.changeGroup(group_Name, false);
  },

  changeGroup: function(group_Name, select){
    for (var group in this._groupList) {
      if (this._groupList[group].groupName == group_Name) {
        for (var layer in this._layers) {
          if (this._layers[layer].group && this._layers[layer].group.name == group_Name) {
            if( select ) {
              this._map.addLayer(this._layers[layer].layer);
            } else {
              this._map.removeLayer(this._layers[layer].layer);
            }
          }
        }
        break;
      }
    }

    this._update();
  },

  _initLayout: function() {
    var className = 'leaflet-control-layers';
    var container = this._container = L.DomUtil.create('div', className);

    // Makes this work on IE10 Touch devices by stopping it from firing a mouseout event when the touch is released
    container.setAttribute('aria-haspopup', true);

    //if (!L.Browser.touch) {
    L.DomEvent.disableClickPropagation(container);
    L.DomEvent.disableScrollPropagation(container);
    L.DomEvent.on(container, 'wheel', L.DomEvent.stopPropagation);
    L.DomEvent.on(container, 'click', L.DomEvent.stopPropagation);
    //} else {
    //  L.DomEvent.on(container, 'click', L.DomEvent.stopPropagation);
    //}

    var section       = document.createElement('section');
    section.className = 'ac-container ' + className + '-list';

    var form = this._form = L.DomUtil.create('form');
    form.id  = 'ac-form';

    // Icon
    var icon       = L.DomUtil.create('div', 'styled-layer-icon', form);
    icon.innerHTML = '&nbsp;'
    L.DomEvent.on(icon, 'click', this._collapse, this);

    // Caption
    var captionHeading    = L.DomUtil.create('h3',   'legend-title', form);
    var captionSpan       = L.DomUtil.create('span', '',             captionHeading);
    captionSpan.innerHTML = 'Controls';

    // Close button
    this._buttonClose = L.DomUtil.create('i', 'fa fa-window-close', form);
    L.DomEvent.on(this._buttonClose, 'click', this._collapse, this);

    section.appendChild(form);

    if (this.options.collapsed) {
      var link   = this._layersLink = L.DomUtil.create('a', className + '-toggle', container);
      link.href  = '#';
      link.title = 'Layers';

      //L.DomEvent.on(link, 'click', this._toggle, this);

      //if (!L.Browser.android) {
      //  L.DomEvent
      //    .on(container, 'click', this._toggle,   this)
      //   // .on(container, 'mouseover', this._expand,   this)
      //   // .on(container, 'mouseout',  this._collapse, this);
      //}

      if (L.Browser.touch) {
        L.DomEvent
          .on(link, 'click', L.DomEvent.stop)
          .on(link, 'click', this._expand, this);
      } else {
        L.DomEvent.on(link, 'focus', this._expand, this);
      }

      this._map.on('click', this._collapse, this);
    } else {
      this._expand();
    }

    //this._collapseControl = L.DomUtil.create('div', className + '-collapse-control', form);

    // Defines where the tile layers group should appear in panel.
    // Set option `base_last: true` so they come bellow overlays.
    if (this.options.base_last) {
      this._overlaysList   = L.DomUtil.create('div', className + '-overlays', form);
      this._baseLayersList = L.DomUtil.create('div', className + '-base',     form);
    } else {
      this._baseLayersList = L.DomUtil.create('div', className + '-base',     form);
      this._overlaysList   = L.DomUtil.create('div', className + '-overlays', form);
    }

    container.appendChild(section);

    // Process options of ac-container css class - to options.container_width and options.container_maxHeight
    for (var c = 0; c < (containers = container.getElementsByClassName('ac-container')).length; c++) {
      if (this.options.container_width) {
        containers[c].style.width = this.options.container_width;
      }

      // Set the max-height of control to y value of map object
      this._default_maxHeight       = this.options.container_maxHeight ? this.options.container_maxHeight : (this._map._size.y - 90);
      containers[c].style.maxHeight = this._default_maxHeight + "px";
    }

    window.onresize = this._on_resize_window.bind(this);
  },

  _on_resize_window: function() {
    // Listen to resize of screen to reajust de maxHeight of container
    for (var c = 0; c < containers.length; c++) {
      // Input the new value to height
      containers[c].style.maxHeight = (window.innerHeight - 90) < this._removePxToInt(this._default_maxHeight) ? (window.innerHeight - 195) + "px" : this._removePxToInt(this._default_maxHeight) + "px";
    }
  },

  // Remove the px from a css value and convert to a int
  _removePxToInt: function(value) {
    if (typeof value === 'string') {
      return parseInt(value.replace("px", ""));
    }

    return value;
  },

  _addLayer: function(layer, name, group, overlay, order) {
    var id = L.Util.stamp(layer.layer);

    this._layers[id] = {
      id      : id,
      layer   : layer.layer,
      name    : layer.description,
      order   : order,
      overlay : overlay
    };

    if (group) {
      var groupId = this._groupList.indexOf(group);

      // if not find the group search for the name
      if (groupId === -1) {
        for (var g in this._groupList) {
          if (this._groupList[g].groupName == group.groupName) {
            groupId = g;
            break;
          }
        }
      }

      if (groupId === -1) {
        groupId = this._groupList.push(group) - 1;
      }

      this._layers[id].group = {
        name           : group.groupName,
        id             : groupId,
        groupElementId : layer.groupElementId,
        expanded       : group.expanded,
        // Load slider initialization values or undefined if not set on layer group
        sliderId       : group.sliderId,
        slider         : group.slider
      };
    }

    if (this.options.autoZIndex && layer.setZIndex) {
      this._lastZIndex++;
      layer.setZIndex(this._lastZIndex);
    }
  },

  _update: function() {
    if (!this._container) {
      return;
    }

    this._baseLayersList.innerHTML = '';
    this._overlaysList.innerHTML   = '';
    this._domGroups.length         = 0;

    var i, obj;
    var baseLayersPresent = false;
    var overlaysPresent   = false;

    // Object to hold each slider and its values
    var sliders = {};

    // Constructor to add values to each slider
    var addSlider = function (sliderId, sliderValue) {
      if (!sliders[sliderId]) {
        // SliderId key is not present yet, initialize a new one
        sliders[sliderId] = [];
      }

      // Append sliderValue to the sliderId key
      sliders[sliderId].push(sliderValue);
    };

    // Define an array for layer sorting
    var sortedLayers = [];
    for (i in this._layers) {
      sortedLayers.push({order:this._layers[i].order,id:this._layers[i].id});
    }

    // Now sort layers according to that one in map layers object
    sortedLayers.sort(function(x,y){return x.order - y.order;});

    for (i = 0; i < sortedLayers.length; i++) {
      obj = this._layers[sortedLayers[i].id];
      this._addItem(obj);

      // Load sliders values if present on layer group
      if (obj.group.slider) {
        var sliderId = obj.group.sliderId;
        sliderValue  = obj.name;

        addSlider(sliderId, sliderValue);

        var self = this;

        this._layerSliders[sliderId] = {
          // Default slider options
          //group          : obj.group.name,
          type             : obj.group.slider.type || single,
          values           : sliders[sliderId],

          // Slider from/to values comes from query string or cookie, or fallback to the values defined in map overlays object
          from             : obj.group.slider.from,
          to               : obj.group.slider.to,
          grid             : obj.group.slider.grid || false,
          prettify_enabled : obj.group.slider.prettify_enabled,
          keyboard         : obj.group.slider.keyboard || false,
          values_separator : obj.group.slider.values_separator || "-",

          // Custom slider options
          sliderId         : sliderId,
          groupIndex       : obj.group.slider.groupIndex,
          groupPrefix      : obj.group.slider.groupPrefix,
          reverse          : obj.group.slider.reverse,
          legend           : obj.group.slider.reverse || false,

          prefix           : obj.group.slider.prefix   || false,
          prettify         : obj.group.slider.prettify || null,

          onChange         : function(data) {
            var layer, checked, index = 0;
            var id = data.input.attr('id');

            self._debug('Activating item ' + data.from_value + ' from slider ' + id + '...');

            // First hide all other layers from the group
            for (var i in self._layers) {
              if (self._layers[i].group.sliderId != undefined && self._layers[i].group.sliderId == id) {

                // The layer we want to be displayed
                if (self._layers[i].name == data.from_value) {
                  index = i;
                }
                else {
                  // Hide the other layers
                  layer   = self._layers[i].layer;
                  checked = self._map.hasLayer(layer);

                  if (checked) {
                    self._map.removeLayer(layer);
                  }
                }
              }
            }

            // Save current value for rendering add addSlider
            self._layerSliders[sliderId].currentFrom = self._layerSliders[sliderId].values.indexOf(String(data.from_value));

            // Add the selected layer
            if (!self._map.hasLayer(self._layers[index].layer)) {
              self._map.addLayer(self._layers[index].layer);
            }
          },
        };
      }

      overlaysPresent   = overlaysPresent   || obj.overlay;
      baseLayersPresent = baseLayersPresent || !obj.overlay;
    }
  },

  _onLayerChange: function(e) {
    var obj = this._layers[L.Util.stamp(e.layer)];

    if (!obj) {
      return;
    }

    if (!this._handlingClick) {
      this._update();
    }

    var type = obj.overlay ?
      (e.type === 'layeradd' ? 'overlayadd'      : 'overlayremove') :
      (e.type === 'layeradd' ? 'baselayerchange' : null);

    if (type) {
      this._map.fire(type, obj);
    }
  },

  // IE7 bugs out if you create a radio dynamically, so you have to do it this hacky way (see http://bit.ly/PqYLBe)
  _createRadioElement: function(name, checked) {
    var radioHtml = '<input type="radio" class="leaflet-control-layers-selector" name="' + name + '"';
    if (checked) {
      radioHtml += ' checked="checked"';
    }
    radioHtml += '/>';

    var radioFragment       = document.createElement('div');
    radioFragment.innerHTML = radioHtml;

    return radioFragment.firstChild;
  },

  _addItem: function(obj) {
    var input, container;

    var label   = document.createElement('div');
    var checked = this._map.hasLayer(obj.layer);
    var objid   = 'ac-' + obj.name.replace(/ /g,'');

    if (obj.overlay) {
      input                = document.createElement('input');
      input.id             = objid;
      input.type           = 'checkbox';
      input.name           = 'leaflet-feature-layers';
      input.className      = 'leaflet-control-layers-selector';
      input.defaultChecked = checked;
      label.className      = "menu-item-checkbox";

      if (obj.group.slider) {
        label.className += ' ionslider-hidden';
      }
    } else {
      input           = this._createRadioElement('leaflet-base-layers', checked);
      input.id        = objid;
      label.className = "menu-item-radio";
    }

    input.layerId = L.Util.stamp(obj.layer);

    L.DomEvent.on(input, 'click', this._onInputClick, this);

    var name = document.createElement('label');
    name.setAttribute("for",objid);

    name.className = 'menu-layer';
    name.innerHTML = '<span>' + obj.name + '</span>';

    label.appendChild(input);
    label.appendChild(name);

    if (obj.layer.StyledLayerControl) {
      // configure the delete button for layers with attribute removable = true
      if (obj.layer.StyledLayerControl.removable) {
        var bt_delete       = document.createElement("input");
        bt_delete.type      = "button";
        bt_delete.className = "bt_delete";

        L.DomEvent.on(bt_delete, 'click', this._onDeleteClick, this);
        label.appendChild(bt_delete);
      }

      // configure the visible attribute to layer
      if (obj.layer.StyledLayerControl.visible){
        this._map.addLayer(obj.layer);
      }
    }

    if (obj.overlay) {
      container = this._overlaysList;
    } else {
      container = this._baseLayersList;
    }

    var groupContainer = this._domGroups[obj.group.id];

    if (!groupContainer) {
      groupContainer    = document.createElement('div');
      groupContainer.id = 'leaflet-control-accordion-layers-' + obj.group.groupElementId;

      if (this._controlState.groupCheckboxes == undefined) {
        this._controlState.groupCheckboxes = {};
      }

      // Verify if type is exclusive
      var s_type_exclusive = this.options.exclusive ? ' type="radio" ' : ' type="checkbox" ';

      // Verify if group requires a slider, then generates it
      // @require JQuery, Ion.RangeSlider
      var slider = '';
      sliderId   = obj.group.sliderId;

      if (obj.group.slider) {
        // Slider container
        slider           = document.createElement('div');
        slider.className = 'menu-item-slider';

        if (obj.group.slider.reverse) {
          slider.className += ' reverse';
        }
        if (obj.group.slider.type == "double") {
          slider.className += ' double';
        }

        // Slider input element
        sliderInput = '<input id="' + sliderId + '" class="ionslider" type="text" />';

        // Append slider input to the container
        slider.innerHTML = sliderInput;

        // Slider legend
        if (obj.group.slider.legend) {
          var legend       = document.createElement('ul');
          legend.className = 'menu-item-slider-legend';

          for (var item in obj.group.slider.legend) {
            legend.innerHTML += '<li><span style="background:' + obj.group.slider.legend[item] + ';"></span>' + item + '</li>';
          }

          slider.innerHTML += legend.outerHTML;
        }
      }

      // Verify if group is expanded
      if (this._controlState.groupCheckboxes['ac' + obj.group.id] == undefined) {
        var s_expanded                                          = obj.group.expanded ? ' checked = "true" ' : '';
        this._controlState.groupCheckboxes['ac' + obj.group.id] = obj.group.expanded ? true                 : false;
      }
      else {
        var s_expanded = this._controlState.groupCheckboxes['ac' + obj.group.id] ? ' checked = "true" ' : '';
      }

      inputElement = '<input id="ac'  + obj.group.id + '" name="accordion-1" class="menu" ' + s_expanded + s_type_exclusive + '/>';
      inputLabel   = '<label for="ac' + obj.group.id + '" class="menu-label">' + obj.group.name + '</label>';

      article = document.createElement('article');
      article.className = 'ac-large';

      // Append slider to article
      if (obj.group.slider) {
        article.className = 'ac-large with-slider';
        article.appendChild(slider);
      }

      article.appendChild(label);

      // Process options of ac-large css class - to options.group_maxHeight property
      if (this.options.group_maxHeight) {
        article.style.maxHeight = this.options.group_maxHeight;
      }

      groupContainer.innerHTML = inputElement + inputLabel;
      groupContainer.appendChild(article);
      container.appendChild(groupContainer);

      // Group expanded/collapsed mechanics
      var self    = this;
      var checked = document.getElementById('ac' + obj.group.id);
      if (checked != null) {
        checked.addEventListener('click', function() {
          self._controlState.groupCheckboxes['ac' + obj.group.id] = self._controlState.groupCheckboxes['ac' + obj.group.id] === true ? false : true;
        });
      }

      // Initialize slider
      if (obj.group.slider) {
        if (this._layerSliders[sliderId] != undefined && this._layerSliders[sliderId].currentFrom != undefined) {
          this._layerSliders[sliderId].from = this._layerSliders[sliderId].currentFrom;
        }

        jQuery('#' + sliderId).ionRangeSlider(this._layerSliders[sliderId]);
      }

      this._domGroups[obj.group.id] = groupContainer;
    } else {
      groupContainer.lastElementChild.appendChild(label);
    }

    return label;
  },

  _onInputClick: function() {
    var i, input, obj;

    var inputs    = this._form.getElementsByTagName('input');
    var inputsLen = inputs.length;

    this._handlingClick = true;

    for (i = 0; i < inputsLen; i++) {
      input = inputs[i];
      obj   = this._layers[input.layerId];

      if (!obj) {
        continue;
      }

      if (input.checked && !this._map.hasLayer(obj.layer)) {
        this._map.addLayer(obj.layer);

      } else if (!input.checked && this._map.hasLayer(obj.layer)) {
        this._map.removeLayer(obj.layer);
      }
    }

    this._handlingClick = false;
  },

  _onDeleteClick: function(obj) {
    var node = obj.target.parentElement.childNodes[0];
    n_obj    = this._layers[node.layerId];

    // Verify if obj is a basemap and checked to not remove
    if (!n_obj.overlay && node.checked) {
      return false;
    }

    if (this._map.hasLayer(n_obj.layer)) {
      this._map.removeLayer(n_obj.layer);
    }

    obj.target.parentNode.remove();

    return false;
  },

  _expand: function() {
    L.DomUtil.addClass(this._container, 'leaflet-control-layers-expanded');
  },

  _collapse: function() {
    if (this._container != undefined) {
      this._container.className = this._container.className.replace(' leaflet-control-layers-expanded', '');
    }
  },

  _toggle: function() {
    if (L.DomUtil.hasClass(this._container, 'leaflet-control-layers-expanded')) {
      this._collapse();
    }
    else {
      this._expand();
    }
  },

  _debug: function(message) {
    if (this.options.debug) {
      console.debug('[Leaflet.StyledLayerControl]', message);
    }
  },
});

L.Control.styledLayerControl = function(baseLayers, overlays, options) {
  return new L.Control.StyledLayerControl(baseLayers, overlays, options);
};
